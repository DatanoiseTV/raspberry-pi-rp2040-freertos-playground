

add_executable(freertos_midi_comm
        freertos_midi_comm.c
        nanomidi/src/nanomidi_stream.c
        nanomidi/src/nanomidi_decoder.c
)

target_include_directories(freertos_midi_comm PRIVATE nanomidi/include)

pico_generate_pio_header(freertos_midi_comm ${CMAKE_CURRENT_LIST_DIR}/uart_rx.pio)
pico_generate_pio_header(freertos_midi_comm ${CMAKE_CURRENT_LIST_DIR}/uart_tx.pio)


pico_set_program_name(freertos_midi_comm "8bitio")
pico_set_program_version(freertos_midi_comm "0.1")

pico_enable_stdio_uart(freertos_midi_comm 0)
pico_enable_stdio_usb(freertos_midi_comm 1)

target_link_libraries(freertos_midi_comm pico_stdlib hardware_pio hardware_adc hardware_irq hardware_dma)
pico_add_extra_outputs(freertos_midi_comm)
