/*
 * FreeRTOS MIDI UART TX/RX Test using PIO.
 */

#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>
#include "pico/stdlib.h"
#include "uart_rx.pio.h"
#include "uart_tx.pio.h"

#include "hardware/gpio.h"
#include "hardware/adc.h"
#include "hardware/irq.h"
#include "hardware/dma.h"

#include "midi_types.h"

#define GPIO 16

char ptrTaskList[250];

struct led_task_arg {
    int gpio;
    int delay;
};

struct midi_rx_task_arg {
    int gpio;
    int baudrate;
};

void led_task(void *p)
{
    struct led_task_arg *a = (struct led_task_arg *)p;

    gpio_init(a->gpio);
    gpio_set_dir(a->gpio, GPIO_OUT);
    while (true) {
        //printf("Hello from Task %d\n", a->gpio);
        gpio_put(a->gpio, 1);
        vTaskDelay(pdMS_TO_TICKS(a->delay));
        gpio_put(a->gpio, 0);
        vTaskDelay(pdMS_TO_TICKS(a->delay));
    }
}


int dma_chan;

void dma_handler() {

    static uint32_t wavetable[32];

    // Clear the interrupt request.
    dma_hw->ints0 = 1u << dma_chan;
    // Give the channel a new wave table entry to read from, and re-trigger it
    dma_channel_set_read_addr(dma_chan, &wavetable[2], true);

}

char dmaBuf[16];
void midiRxTask(void *p){

    struct midi_rx_task_arg *a = (struct midi_rx_task_arg *)p;


    PIO pio = pio0;
    uint offset = pio_add_program(pio, &uart_rx_program);
    uint sm = pio_claim_unused_sm(pio, true);
    uart_rx_program_init(pio, sm, offset, a->gpio, a->baudrate);


    while (true) {
        char c = uart_rx_program_getc(pio, sm);
        printf("SM %i (P%i@%i): %02X\n", sm, a->gpio, a->baudrate, c);
    }
}

void adcReadTask(void *p){
 adc_init();

    // Make sure GPIO is high-impedance, no pullups etc
    adc_gpio_init(26);
    // Select ADC input 0 (GPIO26)
    adc_select_input(0);

    while (1) {
        // 12-bit conversion, assume max value == ADC_VREF == 3.3 V
        const float conversion_factor = 3.3f / (1 << 12);
        uint16_t result = adc_read();
        printf("Raw value: 0x%03x, voltage: %f V\n", result, result * conversion_factor);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void midiTxTask(void *p){
    PIO pio = pio0;

    uint offset = pio_add_program(pio, &uart_tx_program);
    uint sm = pio_claim_unused_sm(pio, true);
    uart_tx_program_init(pio, sm, offset, GPIO, 31250);

    while(true){  
        /*
        uint noteNum = rand() % 127;
        pio_sm_put_blocking(pio, sm, 0x90);
        pio_sm_put_blocking(pio, sm, noteNum);
        pio_sm_put_blocking(pio, sm, rand() % 127);
        vTaskDelay(pdMS_TO_TICKS(500));
        pio_sm_put_blocking(pio, sm, 0x80);
        pio_sm_put_blocking(pio, sm, noteNum);
        pio_sm_put_blocking(pio, sm, rand() % 127);
        */
        pio_sm_put_blocking(pio, sm, 0x42);
        vTaskDelay(pdMS_TO_TICKS(1000));

    }
 
}

void taskList(void *p){
    while(1){
      vTaskList(ptrTaskList);
      printf("Task\t\tState\tPrio\tStack\tNum\n%s\n", ptrTaskList);
      printf("======================================================\n");
      printf("B = Blocked, R = Ready, D = Deleted, S = Suspended\n\n");
      vTaskDelay(pdMS_TO_TICKS(5000));
    }
}


int main()
{
    stdio_init_all();
    


    printf("Start LED blink\n");

    struct led_task_arg arg1 = { 19, 42 };
    xTaskCreate(led_task, "LED_Task 1", 256, &arg1, 1, NULL);

    struct led_task_arg arg2 = { 20, 253 };
    xTaskCreate(led_task, "LED_Task 2", 256, &arg2, 4, NULL);

    struct led_task_arg arg3 = { 25, 441 };
    xTaskCreate(led_task, "LED_Task 3", 256, &arg3, 1, NULL);

    /*
    struct led_task_arg arg4 = { 21, 516 };
    xTaskCreate(led_task, "LED_Task 4", 256, &arg4, 1, NULL);

     struct led_task_arg arg5 = { 22, 420 };
    xTaskCreate(led_task, "LED_Task 5", 256, &arg5, 1, NULL);
    */

    xTaskCreate(midiTxTask, "MIDI TX", 256, NULL, 1, NULL);

    struct midi_rx_task_arg midi_rx_0_arg = {17, 31250};
    xTaskCreate(midiRxTask, "MIDI RX0", 256, &midi_rx_0_arg, 1, NULL);

     struct midi_rx_task_arg midi_rx_1_arg = {18, 31250};
    xTaskCreate(midiRxTask, "MIDI RX1", 256, &midi_rx_1_arg, 1, NULL);

    //xTaskCreate(adcReadTask, "ADC reader", 512, NULL, 1, NULL);

    xTaskCreate(taskList, "Lister", 256, NULL, 1, NULL);

    vTaskStartScheduler();

    while (true)
        ;
}
